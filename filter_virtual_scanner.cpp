#include "filter_virtual_scanner.h"

#include "Octree.h"
#include "SamplingHelper.h"

#include "StarlabDrawArea.h"
#include "RenderObjectExt.h"

// OpenMP
#include <omp.h>

void filter_virtual_scanner::initParameters(RichParameterSet *pars)
{
	pars->addParam(new RichBool("saveFileXYZ", false, "Save points to XYZ file"));
	pars->addParam(new RichBool("addModel", false, "Add as layer"));
	pars->addParam(new RichBool("viz", true, "Visualize"));
	pars->addParam(new RichInt("randSamples", 1e4, "Number of random samples"));
	pars->addParam(new RichBool("faceCenters", false, "Sample only face centers"));
}

void filter_virtual_scanner::applyFilter(RichParameterSet *pars)
{
	SurfaceMeshModel * meshUsed = mesh();
	SurfaceMeshHelper h(meshUsed);
	Vector3VertexProperty points = meshUsed->vertex_property<Vector3>(VPOINT);

	// sample the entire surface
	std::vector<SamplePoint> all_samples;
	if (!pars->getBool("faceCenters"))
	{
		QVector<Vector3> points, normals;
		points = SamplingHelper(mesh()).similarSampling(pars->getInt("randSamples"), normals);
		for (int i = 0; i < (int)points.size(); i++)
			all_samples.push_back(SamplePoint(points[i], normals[i]));
	}
	else
	{
		Vector3FaceProperty fcenters = h.vector3FaceProperty("f:center", Vector3(0, 0, 0));
		Vector3FaceProperty fnormals = meshUsed->face_normals(true);
		foreach(Face f, meshUsed->faces())
		{
			foreach(Vertex v, meshUsed->vertices(f)) fcenters[f] += points[v];
			fcenters[f] /= meshUsed->valence(f);

			all_samples.push_back(SamplePoint(fcenters[f], fnormals[f], 1, f.idx()));
		}
	}

	// visibility
	int N = (int)all_samples.size();
	std::vector<bool> isVisible(N, false);

	// the view direction
	meshUsed->updateBoundingBox();
	Vector3 objCenter = meshUsed->bbox().center();
	qglviewer::Vec camPos = drawArea()->camera()->position();
	Vector3 toScanner(camPos.x, camPos.y, camPos.z);
	toScanner -= objCenter;

	// check visibility using the octree
	Octree octree(meshUsed);
	double noise = 1e-6;
#pragma omp parallel for
	for (int i = 0; i < N; i++)
	{
		const SamplePoint & sp = all_samples[i];
		Eigen::Vector3d rayStart = sp.pos + (toScanner * noise);
		Ray ray(rayStart, toScanner);

		if (octree.intersectRay(ray, noise, true).empty())
		{
			isVisible[i] = true;
		}

	}

	// lock the document before use
	document()->pushBusy();

	// visible samples
	std::vector<SamplePoint> visible_samples;
	for (int i = 0; i < N; i++){
		if (isVisible[i]) 
			visible_samples.push_back(all_samples[i]);
	}

	// visualization + output
	starlab::PointSoup * ps = NULL;
	SurfaceMesh::Model * m = NULL;
	QString newMeshName = QString("%1_sampled").arg(meshUsed->name);
	if (pars->getBool("addModel")) m = new SurfaceMesh::Model(newMeshName + ".obj", newMeshName);
	if (pars->getBool("viz")) ps = new starlab::PointSoup;

	QTextStream * out = NULL;
	QString filename = "";
	if (pars->getBool("saveFileXYZ")) filename = QFileDialog::getSaveFileName(NULL, "Save XYZ", "./", "XYZ file (*.xyz)");
	QFile file(filename);
	if (pars->getBool("saveFileXYZ")){
		file.open(QFile::WriteOnly | QFile::Text);
		out = new QTextStream(&file);
	}

	// Use samples
	for (int i = 0; i < (int)visible_samples.size(); i++)
	{
		const SamplePoint& sp = visible_samples[i];

		if (pars->getBool("viz")) ps->addPointNormal(Vector3(sp.pos), Vector3(sp.n));
		if (pars->getBool("addModel")) m->add_vertex(sp.pos);

		if (pars->getBool("saveFileXYZ")){
			Vector3 p = sp.pos;
			Vector3 n = sp.n;
			(*out) << QString("%1 %2 %3 %4 %5 %6\n").arg(p[0]).arg(p[1]).arg(p[2]).arg(n[0]).arg(n[1]).arg(n[2]);
		}
	}
	file.close();

	// add as layer
	if (pars->getBool("addModel"))
	{
		m->updateBoundingBox();
		document()->addModel(m);
	}

	// visualize
	drawArea()->deleteAllRenderObjects();
	if (pars->getBool("viz"))
	{
		drawArea()->addRenderObject(ps);
	}

	// release the lock
	document()->popBusy();

	drawArea()->updateGL();
}


