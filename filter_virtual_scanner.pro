include($$[STARLAB])
include($$[SURFACEMESH])
include($$[NANOFLANN])
include($$[OCTREE])
StarlabTemplate(plugin)

HEADERS += filter_virtual_scanner.h
SOURCES += filter_virtual_scanner.cpp
