#pragma once

#include "SurfaceMeshPlugins.h"
#include "SurfaceMeshHelper.h"
#include "RichParameterSet.h"

class filter_virtual_scanner : public SurfaceMeshFilterPlugin{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "virtual_scanner.plugin.starlab")
	Q_INTERFACES(FilterPlugin)

public:
	QString name() { return "Virtual Scanner"; }
	QString description() { return "Virtually scan a mesh from a given perspective. Check whether a ray shooting from a surface to the camera intersects the shape volume."; }

	void initParameters(RichParameterSet* pars);
	void applyFilter(RichParameterSet* pars);
};
